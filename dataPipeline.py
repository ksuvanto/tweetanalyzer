from tweepy import Cursor
from authenticator import twitterAuthenticator
from tweetSaver import save_data_csv
from tweetSaver import save_data_json
import json

savetype = "csv"
number_of_tweets = 100

with open("config.json") as json_data_file:
    cfg = json.load(json_data_file)["Finland"]
geocode = cfg["geocode"]
hashtags = cfg["tags"]
language = cfg["language"]

api = twitterAuthenticator()

for tag in hashtags:
    results = []
    for tweet in Cursor(api.search, q=tag, include_entities=True, count=100, lang=language, tweet_mode='extended',  geocode=geocode).items(number_of_tweets):
        results.append(tweet._json)

    if savetype == "json":
        save_data_json(results, tag, "TweetsJSON")
    elif savetype == "csv":
        save_data_csv(results, tag, "TweetsCSV")
    else:
        print("Savetype not supported")


"""
Response JSON content fields:

created_at = timestamp
retweet_count = self explanatory
favorite_count = self explanatory

user = person who retweeted(or tweeted?)
user.location = where user is located(might not exist if user has not added it).

author = empty if tweet is not retweeted(?). Jos on retweetattu niin se on retweettaajan kayttaja sun muuta

retweeted_status = orignal tweet
retweeted_status.user = original tweet user
retweeted_status.favorite_count = might be 0 always?

USE id_str NOT id(reason: https: // stackoverflow.com/questions/2841721/why-a-wrong-tweet-id-is-returned-from-twitter-api)
"""


"""
HASHTAG,
SUB-HASHTAG,
timestamp
username
userId
tweet message
list of followers(id_str, user_names + count)
location(if available)
retweet(info about retweeter)
retweet_count
favorite_count

Identify from the Twitter accounts instances that likely corresponds to pharmaceutical products
and adverts.

"""
