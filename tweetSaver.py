import os.path
import json
import tweepy
from tweepy import Cursor
import pandas as pd
import time
from math import ceil
from datetime import datetime, timedelta
from bs4 import BeautifulSoup


def save_data_csv(twitterAPI, addedUsers, results, fileName, folderToSave=None):

    if (folderToSave is None):
        savePath = fileName + ".csv"
    else:
        if not os.path.exists(folderToSave):
            os.mkdir(folderToSave)
        savePath = folderToSave + "/" + fileName + ".csv"

    id_list = [tweet.id_str for tweet in results]
    data_set = pd.DataFrame(id_list, columns=["tweet_id"])

    # Tweet Data:

    # Check tweet type (tweet/retweet)
    data_set["tweet_type"] = [None for tweet in results]
    data_set["original_tweet_id"] = [None for tweet in results]
    data_set["original_tweetter_id"] = [None for tweet in results]
    data_set["original_tweetter_screen_name"] = [None for tweet in results]
    data_set["searched_hashtag"] = [fileName for tweet in results]
    data_set["sub_hashtags"] = [None for tweet in results]
    data_set["user_mentions_id"] = [None for tweet in results]
    tablePos = 0
    amountOfTweets = 0
    amountOfRetweets = 0
    for tweet in results:
        sub_hashtags = []
        mentions = []
        if tweet.full_text.lower().startswith("rt @"):
            data_set.at[tablePos, "tweet_type"] = "retweet"
            data_set.at[tablePos, "original_tweet_id"] = tweet.retweeted_status.id_str
            data_set.at[tablePos, "original_tweetter_id"] = tweet.retweeted_status.user.id_str
            data_set.at[tablePos,
                        "original_tweetter_screen_name"] = tweet.retweeted_status.user.screen_name
            for tag in tweet.retweeted_status.entities["hashtags"]:
                tag_to_add = '#' + tag['text']
                if tag_to_add.lower() != fileName.lower():
                    sub_hashtags.append(tag_to_add)
            for mention in tweet.retweeted_status.entities["user_mentions"]:
                mentions.append(int(mention["id_str"]))
            amountOfRetweets += 1
        else:
            data_set.at[tablePos, "tweet_type"] = "tweet"
            for tag in tweet.entities["hashtags"]:
                tag_to_add = '#' + tag['text']
                if tag_to_add.lower() != fileName.lower():
                    sub_hashtags.append(tag_to_add)
            for mention in tweet.entities["user_mentions"]:
                mentions.append(int(mention["id_str"]))
            amountOfTweets += 1
        data_set.at[tablePos, "sub_hashtags"] = sub_hashtags
        data_set.at[tablePos, "user_mentions_id"] = mentions
        tablePos += 1

    print("\n\n\n\n\n")
    print("Recieved {} tweets and {} retweets for: {}\n".format(
        amountOfTweets, amountOfRetweets, fileName))
    # if retweet add original tweet id and tweetter id

    data_set["tweet_text"] = [tweet.full_text for tweet in results]
    data_set["tweet_created_at"] = [tweet.created_at for tweet in results]
    data_set["retweet_count"] = [tweet.retweet_count for tweet in results]
    data_set["favorite_count"] = [tweet.favorite_count for tweet in results]

    # User Data:
    data_set["user_id"] = [tweet.user.id_str for tweet in results]  # HOX id_str -> id
    data_set["user_screen_name"] = [tweet.user.screen_name for tweet in results]
    data_set["user_name"] = [tweet.user.name for tweet in results]
    data_set["user_created_at"] = [tweet.user.created_at for tweet in results]
    data_set["user_description"] = [tweet.user.description for tweet in results]
    data_set["user_friends_count"] = [tweet.user.friends_count for tweet in results]
    data_set["user_followers_count"] = [tweet.user.followers_count for tweet in results]
    # Create a list with followers user_ids

    checkETA(results, amountOfTweets)
    data_set["user_follower_list"] = [None for tweet in results]
    tablePos = -1
    for tweet in results:
        tablePos += 1
        ids = []
        if validate(tweet, addedUsers) is True:
            print("\nCollecting followers of {}({}): {}".format(
                tweet.user.name, tweet.user.followers_count, fileName))
            try:
                for page in Cursor(twitterAPI.followers_ids, screen_name=tweet.user.screen_name).pages():
                    ids.extend(page)
                    time.sleep(60)
                    print("Sleeping for a minute")
                data_set.at[tablePos, "user_follower_list"] = ids
                addedUsers.append(tweet.user.id_str)
            except tweepy.TweepError as e:
                print("\n----------------")
                print("ERROR ERROR\n")
                print(e)
                print("\nERROR ERROR")
                print("----------------")
                print("Did not get followers for {}. Sleeping couple minutes just to be safe.\n".format(
                    tweet.user.name))
                time.sleep(120)

    data_set["user_location"] = [tweet.user.location for tweet in results]
    data_set["source"] = [tweet.source for tweet in results]

    data_set.to_csv(savePath, sep=',', encoding='utf-8')
    print("Data saved to {}".format(savePath))


def save_data_json(results, fileName, folderToSave=None):

    if (folderToSave is None):
        savePath = fileName + ".json"
    else:
        if not os.path.exists(folderToSave):
            os.mkdir(folderToSave)
        savePath = folderToSave + "/" + fileName + ".json"

    file = open(savePath, 'w', encoding='utf-8')
    file.write(json.dumps(results))
    file.close()


def save_data_csv_simplified(twitterAPI, addedUsers, results, fileName, folderToSave=None):

    if (folderToSave is None):
        savePath = fileName + ".csv"
    else:
        if not os.path.exists(folderToSave):
            os.mkdir(folderToSave)
        savePath = folderToSave + "/" + fileName + ".csv"

    id_list = [tweet.id_str for tweet in results]
    data_set = pd.DataFrame(id_list, columns=["tweet_id"])

    # Tweet Data:

    # Check tweet type (tweet/retweet)

    data_set["tweet_text"] = [tweet.full_text for tweet in results]
    data_set["tweet_created_at"] = [tweet.created_at for tweet in results]
    data_set["retweet_count"] = [tweet.retweet_count for tweet in results]
    data_set["favorite_count"] = [tweet.favorite_count for tweet in results]

    # User Data:
    data_set["user_id"] = [tweet.user.id_str for tweet in results]  # HOX id_str -> id
    data_set["user_screen_name"] = [tweet.user.screen_name for tweet in results]
    data_set["user_name"] = [tweet.user.name for tweet in results]
    data_set["user_created_at"] = [tweet.user.created_at for tweet in results]
    data_set["user_description"] = [tweet.user.description for tweet in results]
    data_set["user_friends_count"] = [tweet.user.friends_count for tweet in results]
    data_set["user_followers_count"] = [tweet.user.followers_count for tweet in results]
    # Create a list with followers user_ids

    data_set["user_location"] = [tweet.user.location for tweet in results]
    data_set["source"] = [tweet.source for tweet in results]

    data_set.to_csv(savePath, sep=',', encoding='utf-8')
    print("Data saved to {}".format(savePath))

    # Uncomment to save as json also
    # jsonpath = folderToSave + "/" + fileName + ".json"
    # data_set.to_json(jsonpath)


def convert_json_csv(results, fileName, folderToSave=None):

    if (folderToSave is None):
        savePath = fileName + ".csv"
    else:
        if not os.path.exists(folderToSave):
            os.mkdir(folderToSave)
        savePath = folderToSave + "/" + fileName + ".csv"

    id_list = [tweet["id_str"] for tweet in results]
    data_set = pd.DataFrame(id_list, columns=["id_str"])

    # User Data
    data_set["user_id"] = [tweet["user"]["id_str"] for tweet in results]
    data_set["user_screen_name"] = [tweet["user"]["screen_name"] for tweet in results]
    data_set["user_name"] = [tweet["user"]["name"] for tweet in results]
    data_set["user_created_at"] = [tweet["user"]["created_at"] for tweet in results]
    data_set["user_description"] = [tweet["user"]["description"] for tweet in results]
    data_set["user_followers_count"] = [tweet["user"]["followers_count"] for tweet in results]
    data_set["user_friends_count"] = [tweet["user"]["friends_count"] for tweet in results]
    data_set["user_location"] = [tweet["user"]["location"] for tweet in results]

    # Tweet Data
    data_set["text"] = [tweet["full_text"] for tweet in results]
    data_set["retweeted"] = [True if "retweeted_status" in tweet else False for tweet in results]
    # data_set["original_tweet_id"] = [for tweet in results] #TODO: this (path is: tweet["retweeted_status"]["id_str"])
    # data_set["original_tweetter_id"] = # TODO: this (path is: tweet["retweeted_status"]["user"]["id_str"])
    data_set["created_at"] = [tweet["created_at"] for tweet in results]
    data_set["retweet_count"] = [tweet["retweet_count"] for tweet in results]
    data_set["favorite_count"] = [tweet["favorite_count"] for tweet in results]

    data_set["sub_hashtags"] = [None for tweet in results]

    looper = 0
    for tweet in results:
        if ("retweeted_status" in tweet):
            data_set.at[looper, "sub_hashtags"] = tweet["retweeted_status"]["entities"]["hashtags"]
        else:
            data_set.at[looper, "sub_hashtags"] = tweet["entities"]["hashtags"]
        looper += 1

    # data_set["hashtag"] = [tags.pop(tags.index(tag)) for tags in data_set["sub_hashtags"]]
    data_set["source"] = [feedMeHTML(tweet["source"]) for tweet in results]

    data_set.to_csv(savePath, sep=',', encoding='utf-8')


def validate(tweet, listOfAddedIds):  # Check If tweet is retweet and if user already has followr list
    if tweet.full_text.lower().startswith("rt @"):  # if retweet
        return False
    elif tweet.user.id_str in listOfAddedIds:       # if users' followers are already added
        print("\nFollowers of {} already added!".format(tweet.user.name))
        return False
    # elif tweet["user"]["followers_count"] < 1000:  # only process users with at least 1000 followers
    #     return False
    return True


def checkETA(results, amountOfTweets):
    totaltime = 0
    qualifiedUsers = 0
    tmpAddedUsers = []
    for tweet in results:
        if not tweet.full_text.lower().startswith("rt @"):                 # if not retweet
            if tweet.user.id_str not in tmpAddedUsers:               # and not in addedUsers
                tmpAddedUsers.append(tweet.user.id_str)
                qualifiedUsers += 1
                totaltime += ceil((tweet.user.followers_count / 5000) + 0.0001)

    minutes = totaltime % 60
    hours = int((totaltime - minutes) / 60)
    estimateTime = datetime.now() + timedelta(hours=hours, minutes=minutes)
    print("{} out of {} tweets is going to be processed".format(qualifiedUsers, amountOfTweets))
    print("Processing the followers of tweets is going to take about {} hours and {} minutes.".format(hours, minutes))
    print("Estimated time when we are ready: {} ".format(
        estimateTime.strftime('%A')) + '{:%H:%M}'.format(estimateTime))


def feedMeHTML(data):
    return BeautifulSoup(data, features="html.parser").find('a').contents[0]
