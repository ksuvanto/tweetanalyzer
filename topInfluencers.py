""" Task 3:
Identify the top ten influencers in each hashtag in terms of number of tweets generated.
Determine the average and standard deviation of the degree centrality.
"""

import json
import utils
import networkx as nx
from collections import Counter
from plottingTools import plot_Influencers

network = "UaR"

hashtags = ["#COVID19", "#korona", "#koronavirus"]
tweetDict = {}  # Dict to save amount of tweets per
userAndIdDict = {}  # Quick solution to not break Samu's implementation
for tag in hashtags:
    with open("TweetsJSON_2/" + tag + ".json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for tweet in data:
        if "retweeted_status" not in tweet:  # Get just the tweets
            sName = tweet["user"]["screen_name"]
            tweetDict[sName] = tweetDict.get(sName, 0) + 1
            userAndIdDict[sName] = tweet["user"]["id"]

    topInfluencers = dict(Counter(tweetDict).most_common(10))
    userIds = []
    for influencer in topInfluencers.keys():
        userIds.append(userAndIdDict[influencer])
    print("Building a network for hashtag: " + tag)
    G = utils.buildUserAndRetweeterNetwork(tag, False) if network == "UaR" else utils.buildUserAndFollowerNetwork(tag, False)
    G.remove_edges_from(nx.selfloop_edges(G))
    print("Network has " + str(len(G.nodes())) + " nodes")
    print("Network has " + str(len(G.edges())) + " edges")
    print("Calculating average degree and standard deviation for top influencers in hashtag: " + tag)
    averageDegree, std = utils.avgCentralityAndStd(G, userIds)
    print("Average degree is: " + str(averageDegree))
    print("Standard deviation is: " + str(std))
    print("Doing analysing for " + tag + " network")
    analysis = utils.analyzeNetwork(G)
    analysis["topInfluencerAverageDegree"] = averageDegree
    analysis["topInfluencersStd"] = std
    print("Saving " + tag + " network analysis")
    utils.saveAnalytics(analysis, "./analytics", "/" + network + "Network" + tag)
    plot_Influencers(topInfluencers, tag)
