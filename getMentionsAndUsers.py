import json
import os
cfgTag = "tags2"
with open("config.json") as json_data_file:
    cfg = json.load(json_data_file)["Finland"]
hashtags = [t.lower() for t in cfg[cfgTag]]

tweetsRootPath = "" if cfgTag == "tags1" else "TweetsJSON_2/"
tweetsSavePath = "Tweets/MentionsAndUsers"
if not os.path.exists(tweetsSavePath):
    os.mkdir(tweetsSavePath)

for tag in hashtags:
    mentionsAndUsers = {}
    with open(tweetsRootPath + tag + ".json", "r", encoding='utf-8') as f:
        data = json.load(f)
        for row in data:
            if "retweeted_status" in data:
                continue
            for mention in row["entities"]["user_mentions"]:
                if mention["screen_name"] in mentionsAndUsers.keys():
                    mentionsAndUsers[mention["screen_name"]]["users"].append(row["user"]["id"])
                else:
                    mentionsAndUsers[mention["screen_name"]] = {"users": [row["user"]["id"]],
                                                                "mentions_user_id": mention["id"]}
    # max = max(mentionsAndUsers.items(), key = lambda x: len(x[1]["users"]))

    with open(tweetsSavePath + "/" + tag + "MaU" + ".json", 'w', encoding='utf-8') as f:
        f.write(json.dumps(mentionsAndUsers))
