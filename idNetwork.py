import networkx as nx
import json
import matplotlib.pyplot as plt

hashtags = ["#COVID19", "#korona", "#koronavirus"]
#tag = hashtags[1]
G = nx.DiGraph()

users_and_followers = {}
for tag in hashtags:
    tweetsPath = "Tweets/UsersAndRetweeters/" + tag + "UaR.json"
    with open(tweetsPath, "r", encoding='utf-8') as f:
        data = json.load(f)

    for key in data.keys():
        followersForKey = data[key]
        edges = []
        for follower in followersForKey:
            edges.append((str(follower), str(key)))
        G.add_edges_from(edges)
        print("Added node: " + key + " and its followers")

# print("Data length was: " + str(len(data)))
# print("Network has: " + str(G.size()) + " edges")
# print("Network has: " + str(len(G)) + " nodes")
# print("Trying to calculate average shortest path for hashtag: " + tag)
# print(nx.k_nearest_neighbors(G))
# nx.draw_networkx(G)
# plt.show()
#sub_graphs = nx.connected_component_subgraphs(G)
subgraphs = [G.subgraph(c) for c in nx.weakly_connected_components(G)]
print("Number of subgraphs: " + str(len(subgraphs)))
numOfNodes = []
test = 0
for i, sg in enumerate(subgraphs):
    numOfNodes.append(len(sg.nodes()))
    if len(sg.nodes()) == 4408:
        test = sg
numOfNodes.sort(reverse=True)
print(numOfNodes[0:10])
# nx.draw_networkx(test, with_labels=False,node_size=5,width=0.1)
# plt.show()
# print(list(nx.simple_cycles(test)))
# print(nx.dag_longest_path(test))
print(nx.average_clustering(test))
print(nx.average_shortest_path_length(test))
