from tweepy import AppAuthHandler
from tweepy import API
import json

def twitterAuthenticator():
    with open('../permissions.json', 'r') as f:
        keys = json.load(f)

    auth = AppAuthHandler(keys["consumer_key"], keys["consumer_secret"])
    return API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
