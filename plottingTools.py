from matplotlib import pyplot as plt
import networkx as nx

def plot_Influencers(dictToPlot, hashtag):
    names = list(dictToPlot.keys())
    numberOfTweets = list(dictToPlot.values())
    register = list(range(len(numberOfTweets)))
    purple = (0.5, 0.1, 0.9)

    plt.figure(figsize=(12, 6))
    plt.title("Top {} infulencers for hashtag: {}".format(len(names), hashtag), fontsize=16)
    plt.barh(register, numberOfTweets, height=0.6, color=purple)
    plt.yticks(register, names)
    plt.xlabel("Number of tweets")
    for i, v in enumerate(numberOfTweets):  # Draws the value next to the bar
        plt.text(v + .3, i - .1, str(v), color=purple, fontweight='bold')
    plt.show()

def nxGraphVisualizer(G, label=None,title=None):
    nx.draw_networkx(G, with_labels=False,node_size=5,width=0.1)
    plt.xlabel(label)
    plt.title(title)
    plt.show()

if __name__ == '__main__':
    for networkType in ["UaR", "mention"]:
        for tag in ["#COVID19", "#korona", "#koronavirus"]:
            print("Reading and drawing " + networkType + " network for " + tag)
            G = nx.read_multiline_adjlist("./analytics/" + networkType + "Network" + tag + "LargestComponent")
            label = "Largest component in retweet network" if networkType == "UaR" else "Largest component in mention network"
            nxGraphVisualizer(G, label, tag)
            G = nx.read_multiline_adjlist("./analytics/" + networkType + "Network" + tag + "MaxKCoreGraph")
            label = "Max k-core graph in retweet network" if networkType == "UaR" else "Max k-core graph in mention network"
            nxGraphVisualizer(G, label, tag)
