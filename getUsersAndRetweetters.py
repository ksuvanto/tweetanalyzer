import json

# Network where nodes are users and edges mean user has retweeted
# another users tweet
hashtags = ["#COVID19", "#korona", "#koronavirus"]
usersAndRetweeters = {}
for tag in hashtags:
    with open("Tweets/" + "TweetsJSON_2/" + tag + ".json", "r", encoding='utf-8') as f:
        data = json.load(f)
        for row in data:
            if "retweeted_status" in row:
                originalTweeter = row["retweeted_status"]["user"]["id"]
                retweeter = row["user"]["id"]
                if originalTweeter in usersAndRetweeters.keys():
                    usersAndRetweeters[originalTweeter].append(retweeter)
                else:
                    usersAndRetweeters[originalTweeter] = [retweeter]
    with open("./Tweets/usersAndRetweeters/" + tag + "UaR" + ".json", 'w', encoding='utf-8') as f:
        f.write(json.dumps(usersAndRetweeters))
