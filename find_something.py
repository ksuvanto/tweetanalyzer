import json
import re
import requests
'''
If you have created directed graph with Networkx follow as edges.
You can use DiGraph.out_degree to get node's(user's) number of following,
and DiGraph.in_degree node's number of followers.
With these two's ratio possible bots/adverts could be found
'''

hashtags = ["#COVID19", "#korona", "#koronavirus"]
for tag in hashtags:
    with open("TweetsJSON_2/" + tag + ".json", "r", encoding='utf-8') as file:
        data = json.load(file)
    tweetDict = {}
    for tweet in data:
        sName = str(tweet["user"]["screen_name"])
        if sName not in tweetDict:
            tweetDict[sName] = {}
            tweetDict[sName]['followers'] = tweet['user']['followers_count']
            tweetDict[sName]['following'] = tweet['user']['friends_count']
            if ("retweeted_status" not in tweet):  # Get just the tweets
                tweetDict[sName]['tweets'] = 1
                tweetDict[sName]['retweets'] = 0
            else:
                tweetDict[sName]['retweets'] = 1
                tweetDict[sName]['tweets'] = 0
        else:
            if ("retweeted_status" not in tweet):  # Get just the tweets
                tweetDict[sName]['tweets'] = tweetDict[sName]['tweets'] + 1
            else:
                tweetDict[sName]['retweets'] = tweetDict[sName]['retweets'] + 1
    ratioList = []
    for person in tweetDict:
        reTweets = tweetDict[person]['retweets']
        Tweets = tweetDict[person]['tweets']
        followers = tweetDict[person]['followers']
        following = tweetDict[person]['following']
        follow_ratio = following / (followers + 1)
        ratio = follow_ratio * (Tweets + reTweets)
        # if follow_ratio > 20 is decent
        ratioList.append([ratio, person, followers])
    ratioList.sort()
    famous = ratioList[0:5]
    bot = ratioList[-20:-1]
    print()
    print("\n Hashtag:", tag)
    print("\nPossible news Medias or Ministers:")
    for x in famous:
        if x[2] > 100:
            print("Name: {}, Ratio: {}".format(x[1], x[0]))
    print("\nPossible Bots:")
    for x in bot:
        numbers = re.findall('[0-9]+', x[1])
        if numbers:
            if len(numbers[0]) > 2 and x[2] < 100:
                print("Name: {}, Ratio: {}".format(x[1], x[0]))
    print("\n-----------------------------------")
    URL = 'https://www.avoindata.fi/data/fi/api/3/action/datastore_search?q=randomtest&resource_id=08c89936-a230-42e9-a9fc-288632e234f5'
    r = requests.get(url = URL)
    dat = r.json()
    print(dat)
'''
if ratio < 0.01:
    user_string = "User: {} ,ratio: {}, tweets: {}"
    print(user_string.format(person, ratio, Tweets + reTweets))
'''
'''
if (Tweets + reTweets) >= 10 and follow_ratio > 2:
    user_string = "Super_User: {} ,tweets: {}, reTweets: {}, followers: {}, following: {}"
    print(user_string.format(person, Tweets, reTweets, followers, following))
'''
