import networkx as nx
import json
import matplotlib.pyplot as plt
import itertools
import utils


def complete_graph_from_list(L):
    G = nx.Graph()
    if len(L) > 1:
        if G.is_directed():
            edges = list(itertools.permutations(L, 2))
        else:
            edges = list(itertools.combinations(L, 2))
        G.add_edges_from(edges)
    return G


cfgTag = "tags2"
dataRootPath = "Tweets/mentionsAndUsers/"
with open("config.json") as json_data_file:
    cfg = json.load(json_data_file)["Finland"]
hashtags = [t.lower() for t in cfg[cfgTag]]

mention_and_users = {}
for tag in hashtags:
    #tag = "#test"
    dataPath = dataRootPath + tag + "MaU.json"
    G = nx.Graph()
    with open(dataPath, "r", encoding='utf-8') as f:
        data = json.load(f)
        numOfMentions = len(data)
    for key in data.keys():
        mentionGraph = complete_graph_from_list(data[key]["users"])
        G = nx.compose(G, mentionGraph)
        print("Added " + str(len(data[key]["users"])) + " connected nodes for mention: " + key)
        numOfMentions -= 1
        print("Number of mentions left: " + str(numOfMentions))

    # data contained self loops
    G.remove_edges_from(nx.selfloop_edges(G))
    analysis = utils.analyzeNetwork(G)
    utils.saveAnalytics(analysis, "./analytics", "/mentionNetwork" + tag)
    """print("Data length was: " + str(len(data)) )
    print("Network has: " + str(G.size()) + " edges")
    print("Network has: " + str(len(G)) + " nodes")
    subgraphs = [G.subgraph(c) for c in nx.connected_components(G)]
    print("Number of subgraphs: " + str(len(subgraphs)))
    numOfNodes = []
    biggestGraph = 0
    bgSize = -1
    for i, sg in enumerate(subgraphs):
        numOfNodes.append(len(sg.nodes()))
        if (len(sg.nodes()) > bgSize):
            bgSize = len(sg.nodes())
            biggestGraph = sg
    numOfNodes.sort(reverse=True)
    print("Top 3 biggest subgraphs", numOfNodes[0:3])
    print("Biggest subgraph has: " + str(len(biggestGraph.edges())) + " edges")
    print("Biggest subgraph has: " + str(len(biggestGraph.nodes())) + " nodes")
    fName = tag + "MentionNtLargestSubgraph"
    print("Saving biggest subgraph as " + fName)
    nx.write_multiline_adjlist(biggestGraph, fName)

    print("Reading biggest graph from file (for test purpose)")
    biggestGraph = nx.read_multiline_adjlist(fName)
    print("Biggest subgraph has: " + str(len(biggestGraph.edges())) + " edges")
    print("Biggest subgraph has: " + str(len(biggestGraph.nodes())) + " nodes")

    print("Trying to draw biggest subgraph read from file")
    nx.draw_networkx(biggestGraph, with_labels=False,node_size=5,width=0.1)
    plt.show()
    nx.draw_networkx(G, pos=sp, with_labels=False, node_size=35)
    plt.show()
    """
    # print("Data length was: " + str(len(data)))
    # print("Network has: " + str(G.size()) + " edges")
    # print("Network has: " + str(len(G)) + " nodes")
    # subgraphs = [G.subgraph(c) for c in nx.connected_components(G)]
    # print("Number of subgraphs: " + str(len(subgraphs)))
    # numOfNodes = []
    # biggestGraph = 0
    # bgSize = -1
    # for i, sg in enumerate(subgraphs):
    #     numOfNodes.append(len(sg.nodes()))
    #     if (len(sg.nodes()) > bgSize):
    #         bgSize = len(sg.nodes())
    #         biggestGraph = sg
    # numOfNodes.sort(reverse=True)
    # print("Top 3 biggest subgraphs", numOfNodes[0:3])
    # print("Trying to draw biggest subgraph")
    #
    # nx.draw_networkx(biggestGraph, with_labels=False, node_size=5, width=0.1)
    # plt.show()
