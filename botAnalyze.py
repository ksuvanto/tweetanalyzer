import json
from time import strptime
from datetime import date

"""
If you have created directed graph with Networkx follow as edges.
You can use DiGraph.out_degree to get node's(user's) number of following,
and DiGraph.in_degree node's number of followers.
With these two's ratio possible bots/adverts could be found
"""


def numOfDays(date1, date2):
    return (date2 - date1).days


dataCollectedDate = date(2020, 4, 12)  # Add the right time when we collected data
hashtags = ["#COVID19", "#korona", "#koronavirus"]
for tag in hashtags:
    with open("TweetsJSON_2/" + tag + ".json", "r", encoding="utf-8") as file:
        data = json.load(file)
    tweetDict = {}
    print("\nResult for {}:".format(tag))
    for tweet in data:
        sName = str(tweet["user"]["screen_name"])
        if sName not in tweetDict:
            tweetDict[sName] = {}
            if "retweeted_status" not in tweet:  # Get just the tweets
                tweetDict[sName]["tweets"] = 1
                tweetDict[sName]["retweets"] = 0
            else:
                tweetDict[sName]["retweets"] = 1
                tweetDict[sName]["tweets"] = 0
        else:
            if "retweeted_status" not in tweet:  # Get just the tweets
                tweetDict[sName]["tweets"] = tweetDict[sName]["tweets"] + 1
            else:
                tweetDict[sName]["retweets"] = tweetDict[sName]["retweets"] + 1

        createdAt2 = strptime(tweet["user"]["created_at"], "%a %b %d %H:%M:%S +0000 %Y")
        UserCreationDate2 = date(createdAt2.tm_year, createdAt2.tm_mon, createdAt2.tm_mday)
        createdAtDays2 = numOfDays(UserCreationDate2, dataCollectedDate)
        followers = tweet["user"]["followers_count"]
        following = tweet["user"]["friends_count"]
        allTweets = tweet["user"]["statuses_count"]

        tweetDict[sName]["followers"] = followers
        tweetDict[sName]["following"] = following
        tweetDict[sName]["tweetsPerDay"] = round((allTweets / createdAtDays2), 1)
        tweetDict[sName]["createdAtDays"] = createdAtDays2
        tweetDict[sName]["allTweets"] = allTweets
        tweetDict[sName]["created"] = tweet["user"]["created_at"]
        followers = followers if followers > 0 else 1
        following = following if following > 0 else 1
        tweetDict[sName]["followerRatio"] = round(following / followers, 3)

    # Sort dict and check potential bots
    sortedDict = dict(sorted(tweetDict.items(), key=lambda x: x[1]['createdAtDays'], reverse=True))
    for person in sortedDict:

        # To make print statemnet more readable
        createdAt = strptime(sortedDict[person]["created"], "%a %b %d %H:%M:%S +0000 %Y")
        createdAtDays = sortedDict[person]["createdAtDays"]
        allTweets = sortedDict[person]["allTweets"]
        tweetsPerDay = sortedDict[person]["tweetsPerDay"]
        reTweets = sortedDict[person]["retweets"]
        tweets = sortedDict[person]["tweets"]
        followers = sortedDict[person]["followers"]
        following = sortedDict[person]["following"]
        followRatio = sortedDict[person]["followerRatio"]

        # Adjust these properly to get the bots
        if (followRatio + tweetsPerDay) < 50:
            continue

        if followRatio < 5 or tweetsPerDay < 5 or followers > 500:
            continue

        user_string = "User: {:<15} following: {:<6} followers: {:<6} followingRatio: {:<6}" \
            " reTweets: {:<5}  tweets: {:<5} allTweets: {:<8} tweetsPerDay: {:<5} userCreated: {}.{}.{} ({} Days ago)"
        print(user_string.format(person, following, followers, followRatio,
                                 reTweets, tweets, allTweets, tweetsPerDay,
                                 createdAt.tm_mday, createdAt.tm_mon, createdAt.tm_year,
                                 createdAtDays))
