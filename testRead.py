import pandas as pd
import json
import os
from ast import literal_eval as to_list

hashtags = ["#COVID19", "#korona", "#koronavirus"]

for tag in hashtags:
    data = pd.read_csv("TweetsCSV_2/" + tag + ".csv")
    if not os.path.exists("./temp"):
        os.mkdir("./temp")
    if not os.path.exists("./Tweets/UsersAndFollowers/"):
        os.mkdir("./Tweets/UsersAndFollowers/")
    data.to_json("temp/" + tag + ".json")
    userAndFollowers = {}
    with open("temp/" + tag + ".json", "r", encoding='utf-8') as f:
        data = json.load(f)
        for rowNumber in data["user_id"].keys():
            if not data["user_follower_list"][rowNumber]:
                rowList = to_list(data["user_follower_list"][rowNumber])
                if (len(rowList) > 0):
                    userAndFollowers[data["user_id"][rowNumber]] = rowList
    with open("./Tweets/UsersAndFollowers/" + tag + "UaF" + ".json", 'w', encoding='utf-8') as f:
        f.write(json.dumps(userAndFollowers))
