import json
import time
import matplotlib.pyplot as plt

hashtags = ["#COVID19", "#korona", "#koronavirus"]
for tag in hashtags:
    tweet_dates = {}
    tweet_dates2 = []
    with open("../TweetsJSON_2/" + tag + ".json", "r", encoding='utf-8') as file:
        data = json.load(file)
    for tweet in data:
        if ("retweeted_status" not in tweet):  # Get just the tweets
            converted = time.strptime(tweet['created_at'], "%a %b %d %H:%M:%S +0000 %Y")
            date = str(converted.tm_mday)
            #array with all tweet dates.
            tweet_dates2.append(date)
            if date not in tweet_dates:
                tweet_dates[date] = 1
            else:
                tweet_dates[date] = tweet_dates[date] + 1
    #dict seperated to sorted values and keys
    sorted_keys = list(tweet_dates.keys())
    sorted_keys = sorted_keys[::-1]
    sorted_values = []
    for value in sorted_keys:
        sorted_values.append(tweet_dates[value])
    #Invert tweet_dates2, since tweets are in order from newest to oldest
    font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}
    fig = plt.figure(figsize=(12, 6))
    ax = plt.axes()

    plt.rc('font', **font)
    COLOR = 'white'
    plt.rcParams['text.color'] = COLOR
    plt.rcParams['axes.labelcolor'] = COLOR
    plt.rcParams['xtick.color'] = COLOR
    plt.rcParams['ytick.color'] = COLOR
    plt.title(tag + " 3-11.4.2020")
    plt.xlabel("Date")
    plt.ylabel('Tweets')
    ax.set_facecolor('xkcd:black')
    fig.patch.set_facecolor('xkcd:black')
    plt.hist(tweet_dates2[::-1], bins = len(sorted_keys)-1, edgecolor='black', linewidth=1)
    plt.show()
