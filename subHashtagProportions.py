"""We want to investigate the existence of other (sub) hashtags in each of the two collected Twitter dataset in 1).
 Use simple string search in tweet messages to identify the existence of such subhashtags..
Report the results in a table summarizing the proportion of the sub hashtags for each case. """
import json
import matplotlib.pyplot as plt
from numpy import arange
import matplotlib.ticker as mtick

cfgTag = "tags2"
tweetsRootPath = "Tweets/" if cfgTag == "tags1" else "Tweets/TweetsJSON_2/"

with open("config.json") as json_data_file:
    cfg = json.load(json_data_file)["Finland"]
hashtags = [t.lower() for t in cfg[cfgTag]]

def addToHist(key, hist):
    if key not in hist.keys():
        hashtagHist[key] = 1
    else:
        hashtagHist[key] += 1

def plotGraph(normDictHist, numOfTweets, hashtag):
    fig, ax = plt.subplots()
    tags = normDictHist.keys()
    values = normDictHist.values()
    y_pos = arange(len(tags))

    xticks = mtick.FormatStrFormatter(f'%.0f%%')
    ax.xaxis.set_major_formatter(xticks)

    ax.barh(y_pos, values, align='center')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(tags)
    ax.invert_yaxis()
    ax.set_xlabel("Percentage of " + str(numOfTweets) + " total tweets for " + hashtag)
    ax.set_title('Sub-hashtags for ' + hashtag + " with over 1% proportion (03.04.2020 - 11.04.2020)")
    plt.show()
    return;

hists = {}
for tag in hashtags:
    tweetsPath = tweetsRootPath + tag + ".json"
    hashtagHist = {}

    with open(tweetsPath, "r", encoding='utf-8') as f:
        data = json.load(f)
        for row in data:
            if "retweeted_status" not in row:
                addedTags = []
                for t in row["entities"]["hashtags"]:
                    tLower = t["text"].lower()
                    if tLower not in addedTags: # Some people liked to use same hashtag twice
                        addToHist(tLower, hashtagHist)
                        addedTags.append(tLower)
    hists[tag] = hashtagHist
print(hists.keys())
# Normalize data:
normalizedHists = {}
for key in hists.keys():
    totalTweets = hists[key][key[1:]]
    hists[key].pop(key[1:])
    normalizedHists[key] = {}
    for tag in list(hists[key].keys()): # list used to create copy of hists keys
        percentage = round(hists[key][tag] / totalTweets, 2) * 100
        if percentage <= 1:
            hists[key].pop(tag)
        else:
            normalizedHists[key][tag] = percentage
    print(key)
    print(hists[key])
    #plt.rc('font', family='Arial')
    plotGraph(normalizedHists[key], totalTweets, key)
