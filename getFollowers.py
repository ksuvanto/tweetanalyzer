from tweepy import Cursor
import math
from authenticator import twitterAuthenticator
import json
import os.path
import time

with open("config.json") as json_data_file:
    cfg = json.load(json_data_file)["Finland"]
hashtags = cfg["tags"]

saveFile = "userAndFollowers.json"
api = twitterAuthenticator()

userAndFollowers = {}
if os.path.exists(saveFile):
    with open(saveFile, "r", encoding='utf-8') as jsonFile:
        userAndFollowers = json.load(jsonFile)

ids = []

if not os.path.exists("userIds.txt"):
    for tag in hashtags:
        f = open('./' + 'Tweets/' + tag[1:] + 'Tweets.json', 'r', encoding='utf-8')
        jsonData = json.load(f)
        print(tag)
        print(len(jsonData))
        for dataRow in jsonData:
            if "retweeted_status" not in dataRow:
                ids.append(dataRow["user"]["id_str"])
                ids.append(dataRow["user"]["screen_name"])
        f.close()
    with open("userIds.txt", "w", encoding='utf-8') as file:
        file.write(','.join(ids))


with open("userIds.txt", "r", encoding='utf-8') as f:
    # ids = [idLine.split(',') for idLine in f.readlines()][0]
    counter2 = 0
    for idline in f.readlines():
        ids = idline.split(',')
        counter2 += 1
    if counter2 > 1:
        raise ValueError("Id/user list contains multiple rows")


print("this many ids: ", len(ids) / 2)

id = 0
user = ""
deleteIdFromDict = False
requestCounter = 0
try:
    while True:
        print("DO NOT INTERRUPT")
        deleteIdFromDict = False
        user = ids.pop()
        id = ids.pop()
        if id in userAndFollowers:
            print("Added tweet count for user: ", user)
            userAndFollowers[id]["numOfTweets"] += 1
        else:
            followers = []
            deleteIdFromDict = True
            print("SAFE to interrupt")
            print("Adding followers to user: ", user)
            for userId in Cursor(api.followers_ids, user_id=id).items():
                followers.append(userId)
            requestCounter += 1
            userAndFollowers[id] = {
                                    "screenName": user,
                                    "followers": followers,
                                    "numOfFollowers": len(followers),
                                    "numOfTweets": 1}
            print("Added " + str(len(followers)) + " followers to user: " + user)
            print("Manually sleeping...")
            print("If interrupted now the earlier added user and followers will be removed")
            print("and added again when program is started (SAFE TO REMOVE)")
            time.sleep(60 * min(math.ceil(len(followers) / 5000), 15)) # every time wait at least 1min and maximum of 15min
except IndexError:
    print("Finished retrieving followers")
except KeyboardInterrupt:
    print("Interrupting...")
    ids.append(id)
    ids.append(user)
    if (deleteIdFromDict):
        userAndFollowers.pop(id, None)
except Exception as e:
    print("Unexpected error occurred. Interrupting....", e)
    ids.append(id)
    ids.append(user)
    if (deleteIdFromDict):
        userAndFollowers.pop(id, None)
with open(saveFile, 'w', encoding='utf-8') as f:
    f.write(json.dumps(userAndFollowers))
with open('userIds.txt', 'w', encoding='utf-8') as f:
    f.write(','.join(ids))
