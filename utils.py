"""For each hashtag network, provide a short summary of each network
as a table indicating the number of nodes, number of edges, size of giant component.
 Use Networkx function to determine also
 nodes, number of edges, size of giant component
 THE GLOBAL CLUSTERING COEFFICIENT,
THE AVERAGE PATH LENGTH,
   MAXIMUM DEGREE,
   AVERAGE DEGREE,
   MAXIMUM K-CORE NUMBER."""
import networkx as nx
import networkx.algorithms.community as community
import json
import matplotlib.pyplot as plt
import plottingTools
import os
from math import sqrt


def maximumDegree(nAndD):
    return max(nAndD, key=lambda x: x[1])[1]
def minimumDegree(nAndD):
    return min(nAndD, key=lambda x: x[1])[1]

def avgCentralityAndStd(N, nodes):
    nodesAndDegrees = nodes
    if type(nodes[0]) is not tuple:
        nodesAndDegrees = list(N.degree(nodes))
    print(nodesAndDegrees)
    averageDegree = sum([nodeAndD[1] for nodeAndD in nodesAndDegrees]) / len(nodesAndDegrees)
    totalError = sum([(nodeAndD[1] - averageDegree) ** 2 for nodeAndD in nodesAndDegrees])
    std = sqrt(totalError / (len(nodesAndDegrees) - 1))
    return (averageDegree, std)


def analyzeNetwork(N):
    nodes = N.number_of_nodes()
    edges = N.number_of_edges()

    subgraphs = []
    print("Genrating subgraphs...")
    if N.is_directed():
        subgraphs = [N.subgraph(c) for c in nx.weakly_connected_components(N)]
    else:
        subgraphs = [N.subgraph(c) for c in nx.connected_components(N)]

    globalClusteringCoefficient = 0
    avgShortestPathLength = 0
    biggestGraph = nx.Graph()
    nodesAndDegrees = []
    maxKCore = -1
    numOfIndividualNodes = 0
    maxKCoreGraph = nx.Graph()
    print("Number of subgraphs: " + str(len(subgraphs)))
    for i, g in enumerate(subgraphs):
        print("calculating average clustering for subgraph")
        globalClusteringCoefficient += nx.average_clustering(g)
        print("calculating average shortest path length for subgraph")
        shortestPath = nx.average_shortest_path_length(g)
        if shortestPath == 0:
            numOfIndividualNodes += 1
        avgShortestPathLength += shortestPath
        nodesAndDegrees.extend(list(g.degree(list(g.nodes()))))
        if (len(g.nodes())) > len(biggestGraph.nodes()):
            biggestGraph = g

        # nx k_core() function does not require the kcore to be connected
        # documentation seems to be wrong? It looked to be working like documented
        # here: https://stackoverflow.com/questions/22048161/extracting-all-the-k-cores-using-networkx
        # We defined that k-core needs to be a connected subgraph to help with processing time
        print("extracting k_core for subgraph")
        kcore = nx.k_core(g)
        kcoreNumber = minimumDegree(kcore.degree(list(kcore.nodes())))
        if kcoreNumber > maxKCore:
            maxKCore = kcoreNumber
            maxKCoreGraph = kcore
        print("Number of subgraphs processed: " + str(i) + "from total of "  + str(len(subgraphs)))

    globalClusteringCoefficient /= len(subgraphs)
    avgShortestPathLength /= (len(subgraphs) - numOfIndividualNodes)
    print(avgShortestPathLength)

    print("calculating max degree")
    maxDegree = maximumDegree(nodesAndDegrees)
    print("calculating average degree and std")
    averageDegree, std = avgCentralityAndStd(N, nodesAndDegrees)

    return {"nodes": nodes,
            "edges": edges,
            "numberOfSubgraphs": len(subgraphs),
            "globalClusteringCoefficient": globalClusteringCoefficient,
            "avgShortestPathLength": avgShortestPathLength,
            "maxDegree": maxDegree,
            "averageDegree": averageDegree,
            "stdDegree": std,
            "maxKCoreNumber": maxKCore,
            "maxKCoreGraph": maxKCoreGraph,
            "largestComponent": biggestGraph,
            "originalGraph": N
            }


def saveAnalytics(analysis, path, filename):
    if not os.path.exists(path):
        os.mkdir(path)
    nx.write_multiline_adjlist(analysis["largestComponent"], path + filename + "LargestComponent")
    nx.write_multiline_adjlist(analysis["maxKCoreGraph"], path + filename + "MaxKCoreGraph")
    nx.write_multiline_adjlist(analysis["originalGraph"], path + filename + "originalGraph")
    analysis.pop("originalGraph")
    analysis.pop("largestComponent")
    analysis.pop("maxKCoreGraph")
    with open(path + filename + ".json", 'w', encoding='utf-8') as f:
        f.write(json.dumps(analysis))


def buildUserAndFollowersNetwork(hashtag, directed=False):
    G = nx.Graph() if not directed else nx.DiGraph()
    tweetsPath = "Tweets/UsersAndFollowers/" + hashtag + "UaF.json"
    with open(tweetsPath, "r", encoding='utf-8') as f:
        data = json.load(f)

    for key in data.keys():
        followersForKey = data[key]
        edges = []
        for follower in followersForKey:
            edges.append((int(follower), int(key)))
        G.add_edges_from(edges)
    return G

def buildUserAndRetweeterNetwork(hashtag, directed=False):
    G = nx.Graph() if not directed else nx.DiGraph()
    tweetsPath = "Tweets/UsersAndRetweeters/" + hashtag + "UaR.json"
    with open(tweetsPath, "r", encoding='utf-8') as f:
        data = json.load(f)

    for key in data.keys():
        followersForKey = data[key]
        edges = []
        for follower in followersForKey:
            edges.append((int(follower), int(key)))
        G.add_edges_from(edges)
    return G

def readNetwork(hashtag, network, networkType):
    return nx.read_multiline_adjlist("./analytics/" + network + hashtag + networkType, nodetype=int)

def getUser(hashtag, id):
    tweetsPath = "Tweets/TweetsJSON_2/" + hashtag + ".json"
    with open(tweetsPath, "r", encoding='utf-8') as f:
        data = json.load(f)
    for tweet in data:
        if (tweet["user"]["id"] == id):
            return tweet["user"]

def removeNodesByDegree(G, degree):
    g = nx.Graph(G)
    nodesAndDegrees = list(g.degree(list(g.nodes())))
    #nodesAndDegrees.sort(reverse=True, key=lambda x: x[1])
    for node, d in nodesAndDegrees:
        if (d < degree):
            g.remove_node(node)
    #print(nodesAndDegrees[0:5])
    return g

if __name__ == '__main__':
    hashtags = ["#COVID19", "#korona", "#koronavirus"]
    network = "user and follower network"
    """ README: Covid19 network can do k-clique community search no problem (k = 10)
                #Korona network could handle only after removing less than 1000 degree nodes
                but for smaller k values it caused an memory error for my shitty laptop.
                largest k value a community was found for k = 31. (largest degree 40, not that interesting)
                TODO: Try the whole #Korona network for large K values.
    """

    for tag in hashtags:
        print("Building the network...")
        G = buildUserAndFollowersNetwork(tag)
        print("network built")
        #print("Nodes: " + str(len(G.nodes())) + " edges: " + str(len(G.edges())))

        print("Removing nodes that have degree less than 1000")
        # G = removeNodesByDegree(G, 1000)
        # G = nx.read_multiline_adjlist("./" + tag + "UaFnetworkRemovedLessThan" + str(1000) + "degrees", nodetype=int)
        #nx.write_multiline_adjlist(G, "./" + tag + "UaFnetworkRemovedLessThan" + str(1000) + "degrees")
        print("Nodes: " + str(len(G.nodes())) + " edges: " + str(len(G.edges())))

        # G = readNetwork(tag, network, "originalGraph")
        # plottingTools.nxGraphVisualizer(G)
        k = 0
        communities = []
        print("Creating the largest subgraph")
        subgraphs = [G.subgraph(c) for c in nx.connected_components(G)]
        biggestGraph = nx.Graph()
        print(len(subgraphs))
        for i, g in enumerate(subgraphs):
            if (len(g.nodes())) > len(biggestGraph.nodes()):
                print("New larger subgraph found")
                print(len(g.nodes()))
                print(len(g.edges()))
                G = g
                biggestGraph = g
        print("Nodes: " + str(len(G.nodes())) + " edges: " + str(len(G.edges())))
        # For some reason G graph was frozen and only way to unfreeze according to documentation is to create a new graph
        # print("Creating a subgraph with nodes degree less than 3 removed")
        # G = G.subgraph([n[0] for n in nodesAndDegrees])
        r = 0
        if (tag == hashtags[0]):
            r = 10
        elif (tag == hashtags[1]):
            r = 31
        elif (tag == hashtags[2]):
            r = 19
        for i in range(r, 1, -1):
            print(i)
            cliques = nx.find_cliques(G)
            communities = list(community.k_clique_communities(G, i, cliques))
            print(len(communities))
            k = i
            if len(communities) > 0:
                break
        largestCommunity = list(max(communities, key=lambda x: len(x)))
        largestCliqueCommunity = G.subgraph(largestCommunity)
        nodesAndDegrees = list(largestCliqueCommunity.degree(list(largestCliqueCommunity.nodes())))
        maxDegreeAndUser = []
        degree = -1
        while True:
            dAndU = max(nodesAndDegrees, key=lambda x: x[1])
            userId, maxDegree = nodesAndDegrees.pop(nodesAndDegrees.index(dAndU))
            if degree <= maxDegree:
                degree = maxDegree
                maxDegreeAndUser.append((userId, maxDegree))
                if len(nodesAndDegrees) == 0:
                    break
            else:
                break
        users = []
        for id, degree in maxDegreeAndUser:
            user = getUser(tag, int(id))
            print(user["screen_name"], degree)
            users.append(user)
        plottingTools.nxGraphVisualizer(largestCliqueCommunity, tag, network + ", K number = " + str(k))

    """N = nx.path_graph(5)
    N.add_edge(1, 4)
    N.add_edge(6, 7)
    N.add_edge(6, 8)
    N.add_edge(7, 8)

    print(N.edges())
    results = analyzeNetwork(N)
    print(results)
    # utils.saveAnalytics(analysis, "/analytics", "/mentionNetwork" )
    nx.draw_networkx(N)
    plt.show()"""
    # nx.draw_networkx(results["maxKCoreGraph"])
    # plt.show()
    """N = nx.Graph()
    N.add_edge(1,2)
    N.add_edge(1,3)
    N.add_edge(1,4)
    N.add_edge(6,1)"""
